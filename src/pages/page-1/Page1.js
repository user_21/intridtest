import Dropdown from "../../components/dropdown/Dropdown";
import ProductCard from "../../components/productCard/ProductCard";
import { RangeSlider } from "../../components/rangeSlider/RangeSlider";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import "./page1.scss";

const fliterSlidesInfo = [
    {
        label: "Цена, р",
        min: "0",
        max: "5000",
    },
    {
        label: "Наружный диаметр, мм",
        min: "0",
        max: "1000",
    },
    {
        label: "Внутренний диаметр, мм",
        min: "0",
        max: "230",
    },
    {
        label: "Ширина, мм",
        min: "0",
        max: "3000",
    },
];

const filterDropdownInfo = [
    {
        label: "Бренд",
        items: [{ title: "Бренд 1", value: 22 }, { title: "Бренд 2", value: 22 }, { title: "Бренд 3", value: 22 }, { title: "Бренд 4", value: 22 }, { title: "Бренд 5", value: 22 }, { title: "Бренд 6", value: 22 }, { title: "Бренд 7", value: 22 }]
    },
    {
        label: "Материал",
        items: [{ title: "Материал 1", value: 22 }, { title: "Материал 2", value: 22 }, { title: "Материал 3", value: 22 }, { title: "Материал 4", value: 22 }, { title: "Материал 5", value: 22 }, { title: "Материал 6", value: 22 }, { title: "Материал 7", value: 22 }]
    },
];

const navigationInfo = [
    {
        name: "Главная",
        link: "/"
    },
    {
        name: "Уплотнения",
        link: "/"
    }
];

const productCardInfo = [
    {
        items: [
            {
                type: "new",
                text: "Новинка"
            },
            {
                type: "hit",
                text: "Хит"
            },
            {
                type: "stock",
                text: "Акция"
            },
        ],
        img: "1.png",
        price: "351375,25",
    },
    {
        items: [
            {
                type: "ended",
                text: "Нет в наличии"
            },
            {
                type: "hit",
                text: "Хит"
            },
        ],
        img: "2.png",
        price: "351375,25",
    },
    {
        items: [
            {
                type: "new",
                text: "Новинка"
            },
            {
                type: "hit",
                text: "Хит"
            },
            {
                type: "stock",
                text: "Акция"
            },
        ],
        img: "3.png",
        price: "351375,25",
        oldPrice: "450875"
    },
    {
        items: [
            {
                type: "hit",
                text: "Хит"
            },
            {
                type: "stock",
                text: "Акция"
            },
        ],
        img: "1.png",
        price: "351375,25",
    },
    {
        items: [
            {
                type: "new",
                text: "Новинка"
            },
            {
                type: "hit",
                text: "Хит"
            },
            {
                type: "stock",
                text: "Акция"
            },
        ],
        img: "3.png",
        price: "351375,25",
        oldPrice: "450875"
    },
    {
        items: [
            {
                type: "hit",
                text: "Хит"
            },
            {
                type: "stock",
                text: "Акция"
            },
        ],
        img: "4.png",
        price: "351375,25",
        oldPrice: "450875"
    },
    {
        items: [
            {
                type: "new",
                text: "Новинка"
            },
            {
                type: "hit",
                text: "Хит"
            },
        ],
        img: "5.png",
        price: "351375,25",
        oldPrice: "450875"
    },
    {
        items: [
            {
                type: "stock",
                text: "Акция"
            },
        ],
        img: "4.png",
        price: "351375,25",
        oldPrice: "450875"
    },
    {
        items: [
            {
                type: "new",
                text: "Новинка"
            },
        ],
        img: "1.png",
        price: "351375,25",
        oldPrice: "450875"
    },
    {
        items: [
            {
                type: "new",
                text: "Новинка"
            },
        ],
        img: "2.png",
        price: "351375,25",
        oldPrice: "450875"
    },
    {
        items: [

            {
                type: "hit",
                text: "Хит"
            },
            {
                type: "stock",
                text: "Акция"
            },
        ],
        img: "4.png",
        price: "351375,25",
    },
    {
        items: [
            {
                type: "hit",
                text: "Хит"
            },
        ],
        img: "2.png",
        price: "351375,25",
    },
    {
        items: [
            {
                type: "new",
                text: "Новинка"
            },
            {
                type: "stock",
                text: "Акция"
            },
        ],
        img: "1.png",
        price: "351375,25",
        oldPrice: "450875"
    },
    {
        items: [
            {
                type: "new",
                text: "Новинка"
            },
            {
                type: "hit",
                text: "Хит"
            },
            {
                type: "stock",
                text: "Акция"
            },
        ],
        img: "3.png",
        price: "351375,25",
        oldPrice: "450875"
    },
];

const Page1 = () => {

    const [hideFilters, setHideFilters] = useState(false);

    const [isMobile, setIsMobile] = useState(false);

    useEffect(() => {
        const handleResize = () => {
            if (window.innerWidth < 1141) {
                setIsMobile(true);
            } else {
                setIsMobile(false);
            }
        };

        handleResize();

        window.addEventListener('resize', handleResize);

        return () => window.removeEventListener('resize', handleResize);
    });

    return (
        <div className="page">
            <div className="page-wrapper container">
                <div className="page__title-block">
                    <div className="page__title-cnt">
                        <div className="page__nav">
                            {navigationInfo.map((item, index, arr) => {
                                return (
                                    <>
                                        <Link className="page__navlink" to={item.link}>{item.name}</Link>
                                        {index === arr.length - 1 ? "" : " / "}
                                    </>
                                )
                            })}
                        </div>
                        <span className="page__title">Уплотнения поршня гидроцилиндров</span>
                    </div>

                    <div className="page__filters-button" onClick={() => setHideFilters(!hideFilters)}>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3 5H7M21 5H11M3 12H15M21 12H19M3 19H5M21 19H9" stroke="black" stroke-width="2" stroke-linecap="round" />
                            <circle cx="9" cy="5" r="2" stroke="black" stroke-width="2" stroke-linecap="round" />
                            <circle cx="17" cy="12" r="2" stroke="black" stroke-width="2" stroke-linecap="round" />
                            <circle cx="7" cy="19" r="2" stroke="black" stroke-width="2" stroke-linecap="round" />
                        </svg>

                    </div>
                </div>

                <div className="page__content">
                    <div className="filters__background" onClick={e => e.stopPropagation()} style={{ display: hideFilters || !isMobile ? 'flex' : 'none' }}>
                        <div className="filters">
                            <div className="filters__wrapper">
                                <div className="filters__top">
                                    <span className="filters__title">Фильтры</span>
                                    <button className="filters__reset-button" type="button">Сбросить</button>
                                </div>
                                <div className="filters__content">
                                    {fliterSlidesInfo.map((item) => {
                                        return <RangeSlider min={item.min} max={item.max} label={item.label} />
                                    })}

                                    {filterDropdownInfo.map((item) => {
                                        return <Dropdown label={item.label} items={item.items} />
                                    })}
                                </div>
                                <div className="filters__bottom-buttons">
                                    <button className="filters__apply-button">Применить</button>
                                    <button className="filters__reset-button" type="button">Сбросить</button>
                                </div>
                            </div>
                        </div>
                        <span className="filters__close" onClick={() => setHideFilters(false)} />
                    </div>
                    <div className="page__content-main">
                        <div className="page__content-items">
                            {productCardInfo.map((item) => {
                                return (
                                    <ProductCard items={item.items} img={item.img} price={item.price} oldPrice={item.oldPrice} />
                                )
                            })}
                        </div>
                        <div className="page__content-category-desc">
                            <p>
                                Кстати, некоторые особенности внутренней политики представляют собой не что иное,
                                как квинтэссенцию победы маркетинга над разумом и должны быть смешаны с
                                не уникальными данными до степени совершенной неузнаваемости,
                                из-за чего возрастает их статус бесполезности.
                            </p>
                            <p>
                                Банальные, но неопровержимые выводы, а также интерактивные прототипы представлены в
                                исключительно положительном свете. Идейные соображения высшего порядка, а
                                также существующая теория напрямую зависит от вывода текущих активов.
                                Предварительные выводы неутешительны: повышение уровня гражданского сознания
                                не оставляет шанса для форм воздействия. Имеется спорная точка зрения, гласящая
                                примерно следующее: сделанные на базе интернет-аналитики выводы формируют
                                глобальную экономическую сеть и при этом — ассоциативно распределены по отраслям!
                            </p>
                            <p>
                                Высокий уровень вовлечения представителей целевой аудитории является
                                четким доказательством простого факта: семантический
                                разбор внешних противодействий играет определяющее значение
                                для как самодостаточных, так и внешне зависимых концептуальных решений.
                            </p>
                        </div>
                        <iframe src="https://yandex.ru/map-widget/v1/?z=12&ol=biz&oid=19714831601" width="560" height="400" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Page1;