import { Outlet } from "react-router-dom";
import Footer from "../../components/footer/Footer";
import Header from "../../components/header/Header";
import ScrollButton from "../../components/scrollButton/ScrollButton";
import "./main.scss";

const Main = () => {
    return (
        <div className="main-content">
            <Header />
            <div className="page-content-cnt">
                <Outlet />
            </div>
            <Footer />
            <ScrollButton />
        </div>
    )
}

export default Main;