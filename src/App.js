import './App.scss';
import { Routes, Route } from 'react-router-dom';
import Main from './pages/main/Main';
import Page1 from './pages/page-1/Page1';
import Page2 from './pages/page-2/Page2';
import Page3 from './pages/page-3/Page3';

function App() {
    return (
        <Routes>
            <Route path="/" element={<Main />}>
                <Route index element={<Page1 />} />
                <Route path="page-2" element={<Page2 />} />
                <Route path="page-3" element={<Page3 />} />
            </Route>
        </Routes>
    )
}

export default App;
