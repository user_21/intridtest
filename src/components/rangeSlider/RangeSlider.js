import { FC, useCallback, useEffect, useState, useRef } from "react";
import "./rangeSlider.scss";


export const RangeSlider = ({ min, max, label }) => {

    const [minVal, setMinVal] = useState(min);
    const [maxVal, setMaxVal] = useState(max);
    const [inputMinVal, setInputMinVal] = useState(min);
    const [inputMaxVal, setInputMaxVal] = useState(max);
    const minValRef = useRef(null);
    const maxValRef = useRef(null);
    const range = useRef(null);
    const priceGabe = Math.round((+max - +min) / 20);

    const getPercent = useCallback(
        (value) => Math.round(((value - +min) / (+max - +min)) * 100),
        [min, max]
    );

    useEffect(() => {
        if (maxValRef.current) {
            const minPercent = getPercent(+minVal);
            const maxPercent = getPercent(+maxValRef.current.value);

            if (range.current) {
                range.current.style.left = `${minPercent}%`;
                range.current.style.width = `${maxPercent - minPercent}%`;
            }
        }
    }, [minVal, getPercent]);

    useEffect(() => {
        if (minValRef.current) {
            const minPercent = getPercent(+minValRef.current.value);
            const maxPercent = getPercent(+maxVal);

            if (range.current) {
                range.current.style.width = `${maxPercent - minPercent}%`;
            }
        }
    }, [maxVal, getPercent]);

    return (
        <div className="range-slider">
            <div className="range-slider__desc">
                <span className="range-slider__title">{label}</span>
                <div className="range-slider__labels">
                    <span className="range-slider__label">От</span>
                    <span className="range-slider__label">До</span>
                </div>
            </div>

            <div className="range-slider-container">
                <div className="range-slider-container__inputs">
                    <input className="range-slider-container__input range-slider-container__input--left" id="left-input" type="number" value={inputMinVal}
                        onChange={(e) => {
                            if (e.target.value === "") {
                                setInputMinVal("");
                            }
                            else {
                                setInputMinVal(+e.target.value);
                            }
                        }}
                        onBlur={(e) => {
                            if (Math.abs(+maxVal - +inputMinVal) < priceGabe || maxVal < inputMinVal) {
                                setInputMinVal(+maxVal - priceGabe);
                                setMinVal(+maxVal - priceGabe);
                            }
                            else if (inputMinVal === "") {
                                setInputMinVal(minVal);
                            }
                            else {
                                setMinVal(inputMinVal);
                            }
                        }} />
                    <input className="range-slider-container__input" type="number" value={inputMaxVal}
                        onChange={(e) => {
                            if (e.target.value === "") {
                                setInputMaxVal("");
                            }
                            else {
                                setInputMaxVal(+e.target.value);
                            }
                        }}
                        onBlur={(e) => {
                            if (inputMaxVal === "") {
                                setInputMaxVal(maxVal);
                            }
                            else if (Math.abs(+inputMaxVal - +minVal) < priceGabe || inputMaxVal < minVal) {
                                setInputMaxVal(+minVal + priceGabe);
                                setMaxVal(+minVal + priceGabe);
                            }
                            else if (inputMaxVal > max) {
                                setInputMaxVal(max);
                                setMaxVal(max);
                            } else {
                                setMaxVal(inputMaxVal);
                            }
                        }} />
                </div>
                <div className="range-slider-container__slider-cnt">
                    <div className="range-slider-container__slider">
                        <div className="range-slider-container__slider-background">
                            <div ref={range} className="range-slider-container__slider-progress">
                            </div>
                        </div>
                    </div>

                    <div className="range-slider-container__range">
                        <input
                            type="range"
                            className="range-slider-container__range-min"
                            value={minVal}
                            ref={minValRef}
                            min={min}
                            max={max}
                            onChange={(e) => {
                                const value = Math.min(+e.target.value, +maxVal - priceGabe);
                                setMinVal(value);
                                setInputMinVal(value);
                                e.target.value = value.toString();
                            }} />
                        <input
                            type="range"
                            className="range-slider-container__range-max"
                            value={maxVal}
                            ref={maxValRef}
                            min={min}
                            max={max}
                            onChange={(e) => {
                                const value = Math.max(+e.target.value, +minVal + priceGabe);;
                                setMaxVal(value);
                                setInputMaxVal(value);
                                e.target.value = value.toString();
                            }} />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default RangeSlider;
