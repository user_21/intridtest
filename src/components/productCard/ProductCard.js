import "./productCard.scss";

const ProductCard = ({ items, img, price, oldPrice }) => {

    let chooseAnalog = false;

    items.forEach(e => {
        if (e.type === "ended") {
            chooseAnalog = true;
        }
    });

    return (
        <div className={`card-wrapper${chooseAnalog ? "" : " card-wrapper--hovered"}`}>
            <div className="card-cnt">
                <div className="card">
                    <div className="card__stickers">
                        {items.map((item) => {
                            return (
                                <div className={`card__sticker-wrapper card__sticker-wrapper--${item.type}`}>
                                    <div className="card__sticker-item" >
                                        {item.text}
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                    <img className="card__image" src={require(`./images/${img ? img : "1.png"}`)} alt="Фото товара" />
                    <div className="card__desc">
                        <span className="card__title">Котел газ.нап. BAXI POWER HT 1.1200 (WHS43112060-)</span>
                        <div className="card__prop">
                            <span className="card__prop-title">Внутренний диаметр, мм</span>
                            <span className="card__prop-val">22</span>
                        </div>
                        <div className="card__prop">
                            <span className="card__prop-title">Наружный диаметр, мм</span>
                            <span className="card__prop-val">29.2</span>
                        </div>
                        <div className="card__prop">
                            <span className="card__prop-title">Ширина, мм</span>
                            <span className="card__prop-val">3.2</span>
                        </div>
                    </div>
                    <div className="card__price">
                        <span className="card__price-val">{price}₽</span>
                        {oldPrice
                            ? <strike>
                                <span className="card__old-price">{oldPrice}₽</span>
                            </strike>
                            : <></>
                        }
                    </div>
                    <div className="card__basket">
                        {chooseAnalog
                            ?
                            <button className="card__analog-button">
                                Подобрать аналог
                            </button>
                            :
                            <div className="card__basket-cnt">
                                <div className="card__count-panel">
                                    <button className="count-panel__button count-panel__button--minus" type="button" disabled>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
                                            <path d="M5 9.99999C5 9.77898 5.0878 9.56701 5.24408 9.41073C5.40036 9.25445 5.61232 9.16666 5.83333 9.16666H14.1667C14.3877 9.16666 14.5996 9.25445 14.7559 9.41073C14.9122 9.56701 15 9.77898 15 9.99999C15 10.221 14.9122 10.433 14.7559 10.5892C14.5996 10.7455 14.3877 10.8333 14.1667 10.8333H5.83333C5.61232 10.8333 5.40036 10.7455 5.24408 10.5892C5.0878 10.433 5 10.221 5 9.99999Z" fill="#9D9FA5" />
                                        </svg>
                                    </button>
                                    <span className="count-panel__count-val">1</span>
                                    <button className="count-panel__button count-panel__button--plus" type="button">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
                                            <path d="M9.16667 14.1667V10.8333H5.83333C5.61232 10.8333 5.40036 10.7455 5.24408 10.5893C5.0878 10.433 5 10.221 5 10C5 9.77899 5.0878 9.56702 5.24408 9.41074C5.40036 9.25446 5.61232 9.16667 5.83333 9.16667H9.16667V5.83333C9.16667 5.61232 9.25446 5.40036 9.41074 5.24408C9.56702 5.0878 9.77899 5 10 5C10.221 5 10.433 5.0878 10.5893 5.24408C10.7455 5.40036 10.8333 5.61232 10.8333 5.83333V9.16667H14.1667C14.3877 9.16667 14.5996 9.25446 14.7559 9.41074C14.9122 9.56702 15 9.77899 15 10C15 10.221 14.9122 10.433 14.7559 10.5893C14.5996 10.7455 14.3877 10.8333 14.1667 10.8333H10.8333V14.1667C10.8333 14.3877 10.7455 14.5996 10.5893 14.7559C10.433 14.9122 10.221 15 10 15C9.77899 15 9.56702 14.9122 9.41074 14.7559C9.25446 14.5996 9.16667 14.3877 9.16667 14.1667Z" fill="#1E4593" />
                                        </svg>
                                    </button>
                                </div>
                                <button className="card__basket-button">В корзину</button>
                            </div>}
                    </div>
                </div>
                <span className="card__buy">Купить в 1 клик</span>
            </div>
        </div>
    )
}

export default ProductCard;