import { NavLink } from "react-router-dom";
import "./navigation.scss"

const Navigation = ({ items, type, setActiveFunc }) => {
    return (
        <div className={`nav-wrapper ${type}`}>
            <nav className="nav">
                <ul className="nav-list">
                    {items.map((item) => {
                        return (
                            <li>
                                <NavLink
                                    className="nav-link"
                                    activeClass="active"
                                    to={item.href}
                                    onClick={setActiveFunc ? () => setActiveFunc(false) : undefined}
                                >
                                    {item.name}
                                </NavLink>
                            </li>
                        )
                    })}
                </ul>
            </nav>
        </div>
    )
}

export default Navigation;